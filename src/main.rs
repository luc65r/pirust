use std::{
    time::Instant,
    thread,
    sync::mpsc,
    f64::consts::PI
};

use rand::{
    thread_rng,
    distributions::{
        Distribution,
        Standard
    }
};

const POINTS: usize = 1000000000;

fn main() {
    let now = Instant::now();
    let pi = regular_multithreaded(POINTS);
    println!("{}", pi);
    let time = now.elapsed();
    println!("The diff is {}", (PI - pi).abs());
    println!("It took {:?}.", time);
}

// 18.5s 5.40e-5
fn random(points: usize) -> f64 {
    Standard.sample_iter(thread_rng())
        .take(points)
        .map(|(x, y): (f64, f64)|
            if f64::sqrt(x.powi(2) + y.powi(2)) <= 1.0 {4.0} else {0.0})
        .sum::<f64>() / (points as f64)
}

// 3.17s 4.47e-7
fn regular(points: usize) -> f64 {
    let ppl = f32::sqrt(points as f32) as usize;
    (1..ppl + 1).map(|x| (0..ppl).map(|y|
        if f64::sqrt((x as f64 / ppl as f64).powi(2)
            + (y as f64 / ppl as f64).powi(2)) <= 1.0 {4.0} else {0.0}
        ).sum::<f64>()
    ).sum::<f64>() / ppl.pow(2) as f64
}

// 528ms 4.47e-7
fn regular_multithreaded(points: usize) -> f64 {
    let (tx, rx) = mpsc::channel();
    let ppl = f32::sqrt(points as f32) as usize;
    for x in 1..ppl+1 {
        let tx = tx.clone();
        thread::spawn(move || {
            tx.send((0..ppl).map(|y|
                if f64::sqrt((x as f64 / ppl as f64).powi(2)
                    + (y as f64 / ppl as f64).powi(2)) <= 1.0 {4.0} else {0.0}
            ).sum::<f64>()).unwrap();
        });
    };
    (0..ppl).map(|_|
        rx.recv().unwrap()
    ).sum::<f64>() / ppl.pow(2) as f64
}
